package com.example.danieledaliany.trabalhofinalbd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ConexaoBD extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Trabalho.db";
    public static final String TABLE_NAME = "Usuario";

    public static final String COL_1 = "ID";
    public static final String COL_2 = "NOME";
    public static final String COL_3 = "LOGIN";
    public static final String COL_4 = "SENHA";


    public ConexaoBD(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("CREATE TABLE " + TABLE_NAME + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "NOME TEXT, LOGIN TEXT, SENHA TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

    }

    //Método para buscar todos os usuarios do banco sqlite

    public Cursor buscarUsuarios(){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        return res;
    }



    //Método para inserir dados de usuarios no banco sqlite

    public boolean salvarUsuario (String nome, String login, String senha){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_2, nome);
        contentValues.put(COL_3, login);
        contentValues.put(COL_4, senha);

        long result = db.insert(TABLE_NAME, null, contentValues);
        db.close();

        if (result == 1){
            return false;
        } else {
            return true;
        }



    }

    public boolean atualizarUsuario(String id, String nome, String login, String senha){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, nome);
        contentValues.put(COL_3, login);
        contentValues.put(COL_4, senha);

        int result = db.update(TABLE_NAME, contentValues, "ID =?", new String[]{id});

        if (result>0){

            return true;

        } else

        {
            return false;
        }



    }


    public Integer apagarUsuario(String id){

        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete(TABLE_NAME, "ID = ?", new String[] {id});
        return i;


    }

    public Integer Logar(String login,String senha){


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor i = db.rawQuery("SELECT * FROM USUARIO WHERE LOGIN=? AND SENHA=?",new String[]{login,senha});
        if(i.getCount()>0)
        {
            return 1;
        }else{
            return 0;
        }



    }




}
