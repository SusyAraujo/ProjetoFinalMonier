package com.example.danieledaliany.trabalhofinalbd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CadastrarActivity extends AppCompatActivity {

    ConexaoBD myDb;


    EditText edit_Nome, edit_Login, edit_senha;
    Button btn_cadastrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);


        myDb = new ConexaoBD(this);

        edit_Nome = (EditText) findViewById(R.id.inserirNome);
        edit_Login = (EditText) findViewById(R.id.inserirLogin);
        edit_senha = (EditText) findViewById(R.id.inserirSenha);

        btn_cadastrar = (Button) findViewById(R.id.btnCadastrar);

        btn_cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nome = edit_Nome.getText().toString();
                String login = edit_Login.getText().toString();
                String senha = edit_senha.getText().toString();


                //Acionando o método para inserir dados


                if(nome.isEmpty())
                {
                    Toast.makeText(CadastrarActivity.this, "Favor preencher o campo Nome", Toast.LENGTH_SHORT).show();

                }
                else if(login.isEmpty())
                {
                    Toast.makeText(CadastrarActivity.this, "Favor preencher o campo Login", Toast.LENGTH_SHORT).show();

                }
                else if( senha.isEmpty())
                {
                    Toast.makeText(CadastrarActivity.this, "Favor preencher o campo senha", Toast.LENGTH_SHORT).show();

                }
                else {
                    Boolean result =  myDb.salvarUsuario(nome, login, senha);

                    if (result == true){
                        Toast.makeText(CadastrarActivity.this, "Dados inseridos com sucesso", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CadastrarActivity.this, "Erro ao inserir dados", Toast.LENGTH_SHORT).show();
                    }

                }




            }
        });


    }
}



