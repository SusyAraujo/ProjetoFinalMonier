package com.example.danieledaliany.trabalhofinalbd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class UsuarioActivity extends AppCompatActivity {

    ImageView img_visualizar, img_cadastro, img_atualizar, img_apagar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);


        img_visualizar = (ImageView) findViewById(R.id.imgBuscar);
        img_cadastro = (ImageView) findViewById(R.id.imgAdd);
        img_atualizar = (ImageView) findViewById(R.id.imgEdit);
        img_apagar = (ImageView) findViewById(R.id.imgDelete) ;

        img_visualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(UsuarioActivity.this, VisualizarActivity.class);
                startActivity(i);


            }
        });


        img_cadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(UsuarioActivity.this, CadastrarActivity.class);
                startActivity(i);


            }
        });

        img_atualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(UsuarioActivity.this, AtualizarActivity.class);
                startActivity(i);


            }
        });

        img_apagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(UsuarioActivity.this, ApagarActivity.class);
                startActivity(i);


            }
        });

    }
}
