package com.example.danieledaliany.trabalhofinalbd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    ConexaoBD myDb;
    Button btn_acesso,btn_entrar;
    EditText campo_login, campo_senha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        myDb = new ConexaoBD(this);

        campo_login = (EditText) findViewById(R.id.editLogin);
        campo_senha = (EditText) findViewById(R.id.editSenha);


        btn_acesso = (Button) findViewById(R.id.btAcesso);

        btn_acesso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(LoginActivity.this, CadastrarActivity2.class);
                startActivity(i);
            }


        });

        btn_entrar = (Button) findViewById(R.id.btEntrar);

        btn_entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String login = campo_login.getText().toString();
                String senha = campo_senha.getText().toString();


                if (login.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Favor preencher o campo login", Toast.LENGTH_SHORT).show();

                } else if (senha.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Favor preencher o campo semha", Toast.LENGTH_SHORT).show();

                } else {
                    int result = myDb.Logar(login, senha);

                    if (result > 0) {
                        Toast.makeText(LoginActivity.this, "sucesso", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(LoginActivity.this, MenuActivity.class);
                        startActivity(i);
                    } else {
                        Toast.makeText(LoginActivity.this, "Dados invalidos", Toast.LENGTH_SHORT).show();
                    }

                }
            }


        });

    }
}
