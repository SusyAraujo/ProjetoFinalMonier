package com.example.danieledaliany.trabalhofinalbd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AtualizarActivity extends AppCompatActivity {


    ConexaoBD myDb;
    EditText txt_id, txt_nome,txt_login,txt_senha;
    Button btn_atualizar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atualizar);


        myDb = new ConexaoBD(this);

        txt_id = (EditText) findViewById(R.id.atuallizarID);
        txt_nome = (EditText) findViewById(R.id.atualizarNome);
        txt_login = (EditText) findViewById(R.id.atualizarLogin);
        txt_senha = (EditText) findViewById(R.id.atualizarSenha);

        btn_atualizar = (Button) findViewById(R.id.btnAtualizar);

        btn_atualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                ClickMe2();
            }


        });




    }


    private void ClickMe2(){

        String id = txt_id.getText().toString();
        String nome = txt_nome.getText().toString();
        String login = txt_login.getText().toString();
        String senha = txt_senha.getText().toString();

        if( id.isEmpty())
        {
            Toast.makeText(AtualizarActivity.this, "Favor preencher o campo id", Toast.LENGTH_SHORT).show();

        }

        else if(nome.isEmpty())
        {
            Toast.makeText(AtualizarActivity.this, "Favor preencher o campo Nome", Toast.LENGTH_SHORT).show();

        }
        else if(login.isEmpty())
        {
            Toast.makeText(AtualizarActivity.this, "Favor preencher o campo Login", Toast.LENGTH_SHORT).show();

        }
        else if( senha.isEmpty())
        {
            Toast.makeText(AtualizarActivity.this, "Favor preencher o campo senha", Toast.LENGTH_SHORT).show();

        }

        else {
            Boolean result_update = myDb.atualizarUsuario(id, nome, login, senha);

            if (result_update == true) {
                Toast.makeText(this, "Dado atualizado com sucesso", Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(this, "ID não existe", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
