package com.example.danieledaliany.trabalhofinalbd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ApagarActivity extends AppCompatActivity {

    EditText txt_id;
    Button btn_apagar;
    ConexaoBD myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apagar);

        myDb = new ConexaoBD(this);
        txt_id = (EditText) findViewById(R.id.txtApagar);
        btn_apagar = (Button) findViewById(R.id.btnApagar);


        btn_apagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String id = txt_id.getText().toString();
                int result = myDb.apagarUsuario(id);

                if (result != 0){
                    Toast.makeText(ApagarActivity.this, "Usuário deletado", Toast.LENGTH_SHORT).show();
                } else{

                    Toast.makeText(ApagarActivity.this, "Usuário não existe", Toast.LENGTH_SHORT).show();

                }



            }
        });


    }
}
