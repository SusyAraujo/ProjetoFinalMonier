package com.example.danieledaliany.trabalhofinalbd;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class VisualizarActivity extends AppCompatActivity {

    ConexaoBD myDB;
    TextView txt_Resultado;
    Button btn_Consultar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar);

        myDB = new ConexaoBD(this);

        txt_Resultado = (TextView) findViewById(R.id.txtResultado);
        btn_Consultar = (Button) findViewById(R.id.btnConsultar);

        btn_Consultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ClikMe();


            }
        });




    }

    //Método para imprimir os dados existentes no banco sqlite

    public void ClikMe(){

        Cursor res = myDB.buscarUsuarios();
        StringBuffer stringBuffer = new StringBuffer();

        if (res != null && res.getCount()>0){

            //Rodar o banco inteiro até existir dados.
            while (res.moveToNext()){

                stringBuffer.append("Id: "+res.getString(0)+"\n");
                stringBuffer.append("Nome: "+res.getString(1)+"\n");
                stringBuffer.append("Login: "+res.getString(2)+"\n");
                stringBuffer.append("Senha: "+res.getString(3)+"\n \n");

            }
            txt_Resultado.setText(stringBuffer.toString());
            Toast.makeText(this, "Dados lidos com sucesso!", Toast.LENGTH_SHORT).show();


        } else {

            Toast.makeText(this, "Não existe dados no banco!", Toast.LENGTH_SHORT).show();

        }


    }
}

