package com.example.danieledaliany.trabalhofinalbd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MenuActivity extends AppCompatActivity {

    ImageView  img_usuario, img_lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


        img_usuario = (ImageView) findViewById(R.id.imgUsuario);
        img_lista = (ImageView) findViewById(R.id.imgLista);

        img_usuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(MenuActivity.this, UsuarioActivity.class);
                startActivity(i);


            }
        });

        img_lista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(MenuActivity.this, Lista.class);
                startActivity(i);


            }
        });

    }
}
